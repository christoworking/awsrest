from django.conf.urls import url, include
from awsrest.rest.views import SampleView

urlpatterns = [
    url(r'^sample/$', SampleView.as_view(), name='sample-view'),
]